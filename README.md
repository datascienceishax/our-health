# Our Health

This project is a demonstrator on applying systems approach toward the creation of a software.

## Base principles

### Every decision should be justified, and its impact analysed

The aim is not to be as fast as possible, the aim is to be as thorough as possible.

### The working language should be English

The aim is not to be as simple as possible, the aim is to be as comprehensible as possible.

### Everything should be documented

The aim is not to develop functions as fast as possible, 

### Everything should be tested, and tests come first

The aim is not to develop N features in T time, it is to make sure that the N features developed in T time cover all use cases and needs.
Tests definition will stem from the system analysis and functional breakdown.
